package com.hsiu.parallax

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.RelativeLayout


class MyListAdapter(var context: Context?, var nResource: IntArray?, var nScreenHeight: Int?, var nStatusHeight: Int?) : BaseAdapter() {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View?
        val holder: Holder
        if (convertView == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item, parent, false)
            holder = Holder()
            holder.img_test = view!!.findViewById(R.id.img_test)
            holder.rl_root = view.findViewById(R.id.rl_root)
            holder.ll_content = view.findViewById(R.id.ll_content)
            holder.img_test!!.minimumHeight = nScreenHeight!! - this.nStatusHeight!!
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as Holder
        }

        if (position % 2 != 0) {
            holder.ll_content!!.setVisibility(View.VISIBLE)
            val layoutParams: RelativeLayout.LayoutParams
            when (position) {
                1 -> {
                    layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1)
                    holder.rl_root!!.setLayoutParams(layoutParams)
                }
                3 -> {
                    layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 400)
                    holder.rl_root!!.setLayoutParams(layoutParams)
                }
                5 -> {
                    layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, 1200)
                    holder.rl_root!!.setLayoutParams(layoutParams)
                }
                7 -> {
                    layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
                    holder.rl_root!!.setLayoutParams(layoutParams)
                }
            }
            holder.img_test!!.setBackgroundColor(Color.WHITE)
            holder.img_test!!.setResource(0)
        }

        return view
    }

    override fun getItem(position: Int): Any {
        return position

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return nResource!!.size * 2
    }

    internal inner class Holder {
        var img_test: MyListView? = null
        var rl_root: RelativeLayout? = null
        var ll_content: LinearLayout? = null
    }

}